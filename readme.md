# Docker App

Here is the docker compose application to install:
1. MariaDB
2. Mailserver
3. OpenSSO

### Clone this repo
1. Go to srv directory
```bash
$ cd /srv
```

2. Then clone the repo
```bash
$ git clone https://gitlab.com/nanowebdev/example/docker-app.git
```

3. Done


### Usage

**1. MariaDB**

a. Go inside the mariadb dir
```bash
$ cd /srv/docker-app/mariadb
```

b. Edit `docker-compose.yml`, you need to set user information for MariaDB (only once)  
```yml
MYSQL_ROOT_PASSWORD: 'root_password'
MYSQL_DATABASE: 'db_name'
MYSQL_USER: 'user_name'
MYSQL_PASSWORD: 'user_password'
```

c. Then you can deploy or make it down by run this below command.
```bash
# To Deploy 
$ docker compose up -d

# To Down
$ docker compose down
```

### How To Allow Remote Connection
1. Go inside container
```bash
$ sudo docker exec -it [CONTAINER_NAME] bash
```
2. Login to mariadb
```bash
$ mysql -u root -p
```

3. Give permission to the user
```bash
GRANT ALL ON db_name.* to 'user_name'@'%' IDENTIFIED BY 'user_password' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EXIT;
```

4. Exit from database and exit from container
```bash
$ exit
$ exit
```

4. Allow port 3306 to public
```bash
$ ufw allow 3306/tcp
```

5. Done


**2. Mailserver**
```bash
// go inside the mailserver dir
$ cd /srv/docker-app/mailserver
```

a. Edit the `docker-compose.yml`, then edit the hostname  
b. About `environment` you must read to [official documentation](https://docker-mailserver.github.io/docker-mailserver/latest/examples/tutorials/basic-installation/).  
c. Done, then you can deploy it
```bash
# To Deploy 
$ docker compose up -d

# To Down
$ docker compose down
```


**3. OpenSSO**
```bash
# go inside the opensso dir
$ cd /srv/docker-app/opensso
```

a. Make sure you have been edit the `config.js`  
b. Then you can deploy it  
```bash
# To Deploy 
$ docker compose up -d

# To Down
$ docker compose down
```
